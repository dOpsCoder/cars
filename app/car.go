package app

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/look"
)

type CreateCar func(context.Context, domain.Car) error

func NewCreateCar(db domain.CarDBWriter) CreateCar {
	return func(ctx context.Context, c domain.Car) error {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		err := db.Create(ctx, c)
		if err != nil {
			span.Error(err)
			return err
		}

		return nil
	}
}

type ReadCar func(context.Context, domain.CarID) (domain.Car, error)

func NewReadCar(db domain.CarDBReader) ReadCar {
	return func(ctx context.Context, id domain.CarID) (domain.Car, error) {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		c, err := db.Read(ctx, id)
		if err != nil {
			span.Error(err)
			return c, err
		}

		return c, nil
	}
}

type ListCars func(context.Context) ([]domain.Car, error)

func NewListCars(db domain.CarDBReader) ListCars {
	return func(ctx context.Context) ([]domain.Car, error) {
		return db.List(ctx)
	}
}

type UpdateCar func(context.Context, domain.Car) error

func NewUpdateCar(db domain.CarDB) UpdateCar {
	return func(ctx context.Context, car domain.Car) error {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		err := db.Update(ctx, car)
		if err != nil {
			span.Error(err)
			return errors.Wrapf(err, "unable to update car: %s", car.ID)
		}
		return nil
	}
}
