package server_test

import (
	"context"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/cars/server"
	"gitlab.com/jcorry/cars/store"
	"gitlab.com/jcorry/look"
	"gitlab.com/jcorry/look/looktest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

var (
	listen *bufconn.Listener
)

func Test_GRPC_Server(t *testing.T) {
	startTestServer(t)

	ctx, _ := looktest.TestableContext(look.Debug, look.JSON)

	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()

	client := pb.NewCarsClient(conn)

	car := domain.Car{
		Make:     "Ford",
		Model:    "Escort",
		Package:  "base",
		Color:    "white",
		Year:     1987,
		Category: "compact",
		Mileage:  497000,
		Price:    50000,
	}

	t.Run("health", func(t *testing.T) {
		res, err := client.Health(ctx, &pb.HealthRequest{})
		require.NoError(t, err)
		assert.Equal(t, "OK", res.GetMessage())
	})

	t.Run("create a car", func(t *testing.T) {
		req := &pb.CreateRequest{Car: server.ToPb(car)}
		_, err := client.Create(ctx, req)
		require.NoError(t, err)
	})

	var id string

	t.Run("list cars", func(t *testing.T) {
		res, err := client.List(ctx, &pb.ListRequest{})
		require.NoError(t, err)

		for _, c := range res.GetCars() {
			id = c.ID
		}
	})

	t.Run("read car", func(t *testing.T) {
		res, err := client.Read(ctx, &pb.ReadRequest{ID: id})
		require.NoError(t, err)
		c := car
		c.ID = domain.CarID(res.GetCar().GetID())
		assert.EqualValues(t, server.ToPb(c), res.GetCar())
	})

	t.Run("update car", func(t *testing.T) {
		c := car
		c.Color = "red"
		c.ID = domain.CarID(id)
		_, err := client.Update(ctx, &pb.UpdateRequest{ID: string(c.ID), Car: server.ToPb(c)})
		require.NoError(t, err)
	})

	t.Run("read updated car", func(t *testing.T) {
		res, err := client.Read(ctx, &pb.ReadRequest{ID: id})
		require.NoError(t, err)
		c := car
		c.ID = domain.CarID(res.GetCar().GetID())
		c.Color = "red"
		assert.EqualValues(t, server.ToPb(c), res.GetCar())
	})

	t.Run("read car not there", func(t *testing.T) {
		_, err := client.Read(ctx, &pb.ReadRequest{ID: "foo"})
		require.Equal(t, codes.NotFound, status.Code(err))
	})
}

func startTestServer(t *testing.T) {
	// bufconn provides a net.Conn implemented by a buffer
	// https://godoc.org/google.golang.org/grpc/test/bufconn
	listen = bufconn.Listen(1024 * 1024)
	s := grpc.NewServer(interceptors())

	db := store.NewCarDB()
	grpcServer := server.NewServer(db)
	pb.RegisterCarsServer(s, grpcServer)
	go func() {
		if err := s.Serve(listen); err != nil {
			log.Fatal(err)
		}
	}()
}

func bufDialer(ctx context.Context, address string) (net.Conn, error) {
	return listen.Dial()
}

func interceptors() grpc.ServerOption {
	return grpc.ChainUnaryInterceptor(
		look.LogInterceptor,
	)
}
