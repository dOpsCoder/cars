package server

import (
	"context"
	"net"
	"net/http"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.com/jcorry/cars/app"
	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/cars/store"
	"gitlab.com/jcorry/look"
	"google.golang.org/grpc"
)

type Server struct {
	HealthEndpoint Health
	CreateEndpoint Create
	ReadEndpoint   Read
	UpdateEndpoint Update
	ListEndpoint   List
}

// NewServer creates a new Server implementation of the pb server interface configured with
// endpoints.
func NewServer(db *store.CarDB) *Server {
	s := Server{
		HealthEndpoint: NewHealth(),
		CreateEndpoint: NewCreate(
			app.NewCreateCar(db),
		),
		ReadEndpoint:   NewRead(app.NewReadCar(db)),
		UpdateEndpoint: NewUpdate(app.NewUpdateCar(db)),
		ListEndpoint:   NewList(app.NewListCars(db)),
	}

	return &s
}

// Run starts the example gRPC service.
// "network" and "address" are passed to net.Listen.
func Run(ctx context.Context, network, address string, server *Server) error {
	ctx, span := look.OpenSpan(ctx)
	defer span.Close()

	span.Infof("starting gRPC server on network: %s and port: %s", network, address)
	l, err := net.Listen(network, address)
	if err != nil {
		span.Error(err)
		return err
	}
	defer func() {
		if err := l.Close(); err != nil {
			span.Errorf(err, "Failed to close %s", look.Fields{"network": network, "addr": address})
		}
	}()

	s := grpc.NewServer()
	pb.RegisterCarsServer(s, server)

	go func() {
		defer s.GracefulStop()
		<-ctx.Done()
	}()
	return s.Serve(l)
}

// RunInProcessGateway starts the invoke in process http gateway.
func RunInProcessGateway(ctx context.Context, addr string, s *Server, opts ...runtime.ServeMuxOption) error {
	ctx, span := look.OpenSpan(ctx)
	defer span.Close()

	span.Infof("starting gateway on addr: %s", addr)

	gw := runtime.NewServeMux(opts...)
	err := pb.RegisterCarsHandlerServer(ctx, gw, s)
	if err != nil {
		panic(err)
	}
	mux := http.NewServeMux()
	mux.Handle("/", gw)

	h := &http.Server{
		Addr:              addr,
		Handler:           mux,
		ReadHeaderTimeout: 10 * time.Second,
	}

	go func() {
		<-ctx.Done()
		span.Infof("Shutting down the http gateway server")
		if err := h.Shutdown(context.Background()); err != nil {
			span.Errorf(err, "Failed to shutdown http gateway server: %v")
		}
	}()

	if err := h.ListenAndServe(); err != http.ErrServerClosed {
		span.Errorf(err, "Failed to listen and serve: %v")
		return err
	}
	return nil
}
