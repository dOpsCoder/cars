package server

import (
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/cars/pb"
)

func ToPb(c domain.Car) *pb.Car {
	return &pb.Car{
		Make:     c.Make,
		Model:    c.Model,
		Package:  c.Package,
		Color:    c.Color,
		Year:     int32(c.Year),
		Category: c.Category,
		Mileage:  int32(c.Mileage),
		Price:    int32(c.Price),
		ID:       string(c.ID),
	}
}

func ToDomain(c *pb.Car) domain.Car {
	return domain.Car{
		ID:       domain.CarID(c.GetID()),
		Make:     c.GetMake(),
		Model:    c.GetModel(),
		Package:  c.GetPackage(),
		Color:    c.GetColor(),
		Year:     int(c.GetYear()),
		Category: c.GetCategory(),
		Mileage:  int(c.GetMileage()),
		Price:    int(c.GetPrice()),
	}
}
