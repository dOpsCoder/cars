package server

import (
	"context"

	"gitlab.com/jcorry/cars/app"
	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/look"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type List func(context.Context, *pb.ListRequest) (*pb.ListResponse, error)

func NewList(handler app.ListCars) List {
	return func(ctx context.Context, req *pb.ListRequest) (*pb.ListResponse, error) {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		cars, err := handler(ctx)
		if err != nil {
			span.Error(err)
			return nil, status.Error(codes.Internal, err.Error())
		}

		var r []*pb.Car

		for _, c := range cars {
			r = append(r, ToPb(c))
		}

		return &pb.ListResponse{Cars: r}, nil
	}
}

func (s *Server) List(ctx context.Context, req *pb.ListRequest) (*pb.ListResponse, error) {
	return s.ListEndpoint(ctx, req)
}
