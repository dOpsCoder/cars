
lint:
	golangci-lint run ./...
run-server:
	go run ./cmd/server/main.go

run-client:
	go run ./cmd/grpc-client/main.go

generate:
	buf generate
install:
	go mod tidy && \
	go install \
        github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
        github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
        google.golang.org/protobuf/cmd/protoc-gen-go \
        google.golang.org/grpc/cmd/protoc-gen-go-grpc