[![codecov](https://codecov.io/gl/jcorry/cars/branch/main/graph/badge.svg?token=RT21IZKJAH)](https://codecov.io/gl/jcorry/cars)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jcorry/cars)](https://goreportcard.com/report/gitlab.com/jcorry/cars)
[![wakatime](https://wakatime.com/badge/user/93f3aa28-826d-4f50-a9f3-73fde2d9cd45/project/72d06aed-eb23-48c2-96b7-a24a1dc71c41.svg)](https://wakatime.com/badge/user/93f3aa28-826d-4f50-a9f3-73fde2d9cd45/project/72d06aed-eb23-48c2-96b7-a24a1dc71c41)

# cars

[Why I did this the way I did](explanation.md)

[![codecov](https://codecov.io/gl/jcorry/cars/branch/main/graphs/sunburst.svg?token=RT21IZKJAH)](https://codecov.io/gl/jcorry/cars)

## Coding Challenge
In preparation for our technical interview, we&#39;d like you to implement a very basic microservice
consisting of an OpenAPI contract, Golang API, and appropriate testing code for finding, listing
and updating information about &quot;cars&quot;. Please be prepared to share your work during the
technical interview, including discussion around the “why” behind the decisions you made or
did not make. We are not looking for “one perfect solution”, we are looking to see how you
break a problem down and implement it, including planning, executing, and future states.
For demoing this exercise, please make your API runnable against a database instance of your
choice.
## API
We'd like to see the application host four HTTP endpoints, returning JSON where applicable:
1. GET endpoint to retrieve an existing car
2. GET endpoint to retrieve a list of cars
3. POST endpoint to create a new car
4. PUT endpoint to update an existing car

We can assume that this is an internal API that has already been authenticated and authorized
and do not need to worry about security in that sense.
## Contract
Please document your API using an OpenAPI contract.
## Observability
Observability is an important part of an application. Implement some aspect of observability
whether it is logging, metrics, or tracing, but be prepared to discuss the topic in depth.
## Tests
Please include some automated testing, but be prepared to discuss the topic in depth.


## Getting started

1. pull the repo
2. CD to the `cars` directory that was created
3. `make run-server` to run the server
4. `make run-client` to run a client demo of the `health` endpoint

Once the server is running, you can make HTTP requests on the default port :8080. If you want to change the ports the servers run on you can.

`go run ./cmd/server/main.go -addr=:9191 -gwaddr=:8181`

The client also accepts an `addr` flag to specify the address.

The `protos/cars.proto` file describes the service, there is also an OpenAPI spec in the `swagger` directory. Consumers are encouraged to follow the pattern presented in `cmd/grpc-client/main.go` which provides a (minimal) example of how to use the gRPC client.

There are some tools to install if you want to regenerate the proto files, most of these can be 
installed with `make install`. You'll need `Buf` which I installed with `brew install buf`