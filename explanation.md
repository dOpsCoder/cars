## Personal Development

I love building things.

Most of the things I build have constraints on their way of being built that drain some of the joy of building from the process.

I love take home interview exercises because they give me the opportunity to build something from a prompt, entirely unconstrained and in accordance to my own values and ideals. I get the opportunity to experiment and try things and thus learn and grow. Another effect is that I ultimately wind up with a demo application in my portfolio that I can use to showcase or highlight skills or principles that I wish to be known for.

For those reasons, I tend to overdo it a little on these things.

For an overview of the development process, take a look at the [gitlab merge requests](https://gitlab.com/jcorry/cars/-/merge_requests), they tell the story of the development of the application.

## Focus on approach

I emphasized the approach, rather than the implementation. In this case, the exercise requires the building of a simple, RESTful API where cars are the resource. I have been building RESTful APIs for years so the task was very familiar and known.

I am an advocate for gRPC as a REST replacement. I gave a talk on this at the Go meetup a few years ago. One of the biggest hurdles to evangelizing gRPC as a valid API implementation is in teaching people about the tooling required. The documentation for each of the libraries and tools is a little daunting and hard to unify. Buf seeks to build this unification of tools behind a usable, fluent interface. I haven't had any meaningful experience with Buf so wanted to use this exercise as a means of getting some experience.

I looked at this simple little API exercise as an opportunity to experiment and see if I can satisfy the RESTful requirements using grpc-gateway and openapiv2 components of the Buf (protoc) tooling. Walking through how to do that has given me some really useful experience that I will be able to share with others and bring to future service development projects. I was very happy to discover it was a lot easier than I anticipated.

## Describing an API

It's imperative that our APIs be usable. Our traditional means of ensuring usability has been to document our RESTful APIs using OpenAPI (often known as Swagger). One purpose of the documentation is to communicate between service producers and service consumers how to use the service. What do requests look like? What do responses look like? What addresses can requests be made to get responses? What options are available? How does auth work?

OpenAPI provides a standard for communicating all of this in a structured yml format that can be parsed and displayed in a UI and it is pretty great.

This doesn't go far enough, but it provides a hint.

Documentation must be read, interpreted and understood. It also becomes completely useless at the point where it diverges from how the software actually works. Even if it is 95% accurate, the 5% inaccuracy makes it untrustworthy and unbelievable, which render it 100% useless.

If OpenAPI can be _parsed_ for the purpose of generating a doc viewer UI...then it can be parsed to generate _code_. _This_ is a great use of OpenAPI! Document your API, describe it in yaml (cheap and easy to work with) then generate your client and server interfaces from the documentation you wrote! Genius! This provides several benefits:

- The service and documentation are linked and will not diverge, the docs will always reflect the service because the service is generated from the documentation.
- Consumers can generate client code from the same documentation, giving them a reliable SDK for consuming the service. Or...as a project owner you can generate the SDKs in common languages.

Now we are cooking with gas!


## Protobuf > OpenAPI

Protobuf is a much more exact, specific and concise IDL (interface description language) than OpenAPI.

OpenAPI presumes JSON as a transport layer, Protobuf uses binary encoding.

Protobuf has a much richer set of type primitives than OpenAPI (ie: `double`, `float`, `int32`, `int64`, `bytes`) If you have struggled with the conversions and evaluations that are inherent in your request value being a `number`, you will love this.

The binary encoding allows for dramatically increased API performance in 2 key ways:

- JSON decoding/encoding is _far more expensive_ than Protobuf serialization/deserialization
- Protobufs serialized binary representation is far smaller than JSON ASCII encoding and so request/response payloads are much smaller.

Performance matters. Moving bytes over networks is slow and expensive. Protobufs grossly outperform JSON here.

The IDL is very feature rich and reads more like software than like text. As a programmer, I find myself lost in a sea of yaml when writing OpenAPI. Proto files are way more like pseudocode. They give me much more precise expression of how the service should work in many fewer lines/characters.

## gRPC > REST

A service is comprised of RPCs (functions) that accept requests and return responses. The gRPC implementation in Go is really pleasant to work with. I get a server interface that I implement with handlers. This is pretty straightforward and very similar to building HTTP APIs. The biggest difference is that the error handling and type conversions from internal application types to presentation layer types are more precise and "go like".

If you have ever set a `*string` value in a response type, you will love this.

Take a look at `server/car.go` to see my helper function for converting an internal representation of a car (`domain.Car`) to a presentation layer representation `*pb.Car`

## Layered architecture

Application structure and layout is where most new Go programmers struggle. Examples available online purporting to be "standard" are anything but. Popular frameworks encourage some bad architecture by presenting minimally functioning demos of how to use them.

We should insist on a layered architecture.

- **ports/presentation**: This is the ingress point. Each request is mapped to a handler here. Request and response values are used to call functions further down the stack.

- **application**: business logic layer. You _do things_ here. Maybe the things you do are asking for data from a data store, or making calls to external services, or calculating values, or building up transactions to execute or whatever your software needs to do. The application is _your_ application. Keep it separate from it's presentation. Functions in the application layer may be called by a REST or gRPC service endpoint handler...or may be called by a CLI command...or a AMQP processor...or in a AWS Lambda. If you can keep this entirely separate from the presentation layer, you'll maintain the flexibility for all of these cases and create an environment where the tasks related to service request/response can be done independently of core application logic work.

- **infrastructure**: This is most commonly where your data persistence lives. Infrastructure provides code for interacting with resources: dbs, filesystems, cloud resources, 3rd party APIs, etc. Your application may depend on types/values from this layer and require them in order to work. In many cases, it is helpful to describe these resources as application layer interfaces, and then provide implementations here in the infra layer.

"wtf?" you might say. That's an awful lot of abstraction and separation of concerns for a simple single resource REST API.

Yes...it is.

The alternative to this approach always, ever only leads to a tangled mess of dependencies that pile upon one another in ever expanding functions as the application grows.

Go slow so you can go fast.

## Minimal data implementation

While the prompt for this exercise gave some hints about the data persistence layer, I chose not to focus here. I have plenty of DB experience and that just wasn't where I wanted to spend my time. I really wanted to spend the time getting Buf to build grpc and grpc-gateway types and run a gRPC server concurrently with a REST/grpc-gateway server in the simplest way possible...that's where my time went.

I built a simple in memory data store for the cars. The only indexing requirement was that they be retrievable by ID...so a simple `map[string]domain.Car` was adequate as a storage container. The functions for reading/writing are described in interfaces in the domain package and those are passed to dependencies. This will allow easily re-implementing the storage layer with postgres or SQLite or something later.

At that point, the schema can be defined, you would also want to:

- normalize the data and save make, model, category and maybe color data in separate tables.
- build the DB in a docker container that can run alongside the app
- run a DB in a test container for integration testing

Basic, normal DB stuff

# Observability

I used my own structured logging package: https://gitlab.com/jcorry/look

I love Datadog and if this went to production I'd probably add the ddog agent and set up some filters/dashboards there to analyse and present logs. Capturing metadata on log lines allows for a LOT of information to be derived from the log data and gives a pretty high ROI so is a good starting point

I also like to use Codecov to observe changes to test coverage and watch trends there and give developers a visual indication of where test coverage is strong and where it needs help.

# CI

Continuous integration is critical! Any changes should be tested and prevented from merging to main until some conditions are met. Tests must be run and pass. The linter must find no formatting problems. The code must build. It's not responsible to present code as "done" unless and until you have some reasonable assurances that it works.