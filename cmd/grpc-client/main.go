package main

import (
	"context"
	"flag"
	"log"

	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/look"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	addr = flag.String("addr", ":9090", "endpoint of the gRPC service")
)

func main() {
	// Set up logger
	logger := look.NewStdLogger(look.LogLevel(look.Info), look.JSON) // default "0" is ERROR (i.e. nothing but errors)
	ctx := look.CtxWithLogger(context.Background(), logger)

	ctx, span := look.OpenSpan(ctx)
	span.Info("starting gRPC client...")

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	conn, err := grpc.Dial(*addr, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	client := pb.NewCarsClient(conn)

	// health check
	res, err := client.Health(ctx, &pb.HealthRequest{})
	if err != nil {
		span.Error(err)
	}
	span.Infof("Health: %s", res.GetMessage())
}
