package domain

import (
	"context"
	"errors"
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate
//counterfeiter:generate . CarDBWriter
type CarDBWriter interface {
	Create(context.Context, Car) error
	Update(context.Context, Car) error
}

//counterfeiter:generate . CarDBReader
type CarDBReader interface {
	Read(context.Context, CarID) (Car, error)
	List(context.Context) ([]Car, error)
}

//counterfeiter:generate . CarDB
type CarDB interface {
	CarDBReader
	CarDBWriter
}

type CarID string

type Car struct {
	ID       CarID
	Make     string
	Model    string
	Package  string
	Color    string
	Year     int
	Category string
	Mileage  int
	Price    int
}

func (c Car) Validate() error {
	if len(c.ID) != 27 {
		return errors.New("invalid ID")
	}
	return nil
}
